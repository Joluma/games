# JOJO GAME CENTER

![logo](logo.jpg)

## Présentation

Une série de petits jeux entièrement faits en javascript, vanilla JS pour la plupart mais peut-être certains seront réalisés à l'aide d'un framework.

## Jeux

#### Memory

[https://joluma.gitlab.io/games/memory](https://joluma.gitlab.io/games/memory)

Le célèbre jeux du memory. Des cartes sont retournées, on doit les retourner 2 par deux jusqu'à ce qu'on ai retourné toutes les paires en même.
