class MatchGame {
  constructor(gameBoard) {
    this.numberOfPairs = 8;
    this.flippedCards = [];
    this.flippedPairs = 0;
    this.numberOfPlays = 0;
    this.gameBoard = gameBoard;

    this.colors = [
      '#39add1', // light blue
      '#3079ab', // dark blue
      '#c25975', // mauve
      '#e15258', // red
      '#f9845b', // orange
      '#838cc7', // lavender
      '#7d669e', // purple
      '#53bbb4', // aqua
      '#51b46d', // green
      '#e0ab18', // mustard
      '#637a91', // dark gray
      '#f092b0', // pink
      '#b7c0c7', // light gray
      '#f0c27b', // kind of yellow
      '#4b1248', // violet
      '#add100', // light green
      '#7b920a', // green
      '#bb377d'  // dark pink
    ];

    this.matchCss = {
      backgroundColor: 'rgb(150, 150, 150)',
      color: 'rgb(200, 200, 200)'
    };

    this.startGame();
  }

  setDifficulty(difficulty) {
    this.numberOfPairs = difficulty;
    this.startGame();
  }

  startGame() {
    this.flippedPairs = 0;
    this.numberOfPlays = 0;
    this.cardValues = this.generateCardValues();
    this.renderCards();
  }

  generateCardValues() {
    const sequentialValues = [];

    for (let value = 1; value <= this.numberOfPairs; value++) {
      sequentialValues.push(value);
      sequentialValues.push(value);
    }

    const cardValues = [];

    while (sequentialValues.length > 0) {
      const randomIndex = Math.floor(Math.random() * sequentialValues.length);
      const randomValue = sequentialValues.splice(randomIndex, 1)[0];
      cardValues.push(randomValue);
    }

    return cardValues;
  }

  renderCards() {
    this.flippedCards = [];

    this.gameBoard.innerHTML = '';

    for (let valueIndex = 0, ln = this.cardValues.length; valueIndex < ln; valueIndex++) {
      const value = this.cardValues[valueIndex];
      const color = this.colors[value - 1];
      const data = {
        value: value,
        color: color,
        isFlipped: false
      };

      const $cardElement = document.createElement('div');
      $cardElement.classList.add('card');
      $cardElement.dataset.value = data.value;
      $cardElement.dataset.color = data.color;
      $cardElement.dataset.isFlipped = data.isFlipped;

      this.gameBoard.appendChild($cardElement);
    }

    document.querySelectorAll('.card').forEach(card => {
      card.addEventListener('click', () => {
        this.flipCard(card);
      });
    });
  }

  flipCard($card) {
    if ($card.dataset.isFlipped === 'true') {
      return;
    }

    $card.style.backgroundColor = $card.dataset.color;
    $card.textContent = $card.dataset.value;
    $card.dataset.isFlipped = true;

    this.flippedCards.push($card);

    if (this.flippedCards.length === 2) {
      this.numberOfPlays++;

      if (this.flippedCards[0].dataset.value === this.flippedCards[1].dataset.value) {
        this.flippedCards[0].style.backgroundColor = this.matchCss.backgroundColor;
        this.flippedCards[0].style.color = this.matchCss.color;

        this.flippedCards[1].style.backgroundColor = this.matchCss.backgroundColor;
        this.flippedCards[1].style.color = this.matchCss.color;

        this.flippedPairs++;
        this.checkVictory();
      } else {
        const card1 = this.flippedCards[0];
        const card2 = this.flippedCards[1];

        window.setTimeout(function() {
          card1.style.backgroundColor = 'rgb(32, 64, 86)';
          card1.textContent = '';
          card1.dataset.isFlipped = false;

          card2.style.backgroundColor = 'rgb(32, 64, 86)';
          card2.textContent = '';
          card2.dataset.isFlipped = false;
        }, 350);
      }

      this.flippedCards = [];
    }
  }

  checkVictory() {
    if (this.flippedPairs === this.numberOfPairs) {
      alert(`VICTOIRE en ${this.numberOfPlays} coups !!!`);
    }
  }
}

(function() {
    const $game = document.querySelector('#game');
    const matchGame = new MatchGame($game);
    const difficulties = document.menuForm.difficulty;
    const startButton = document.querySelector('.start-button');

    startButton.addEventListener('click', function() {
      matchGame.startGame();
    });

    difficulties.forEach(difficulty => {
      difficulty.addEventListener('change', function() {
        matchGame.setDifficulty(parseInt(this.value));
      });
    });
})();
